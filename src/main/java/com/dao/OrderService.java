package com.dao;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import com.model.Order;
//
//import java.util.List;
//
//@Service
//public class OrderService {
//
//    @Autowired
//    private OrderRepository orderRepository;
//
//    public Order createOrder(Order order) {
//        return orderRepository.save(order);
//    }
//
//    public List<Order> getAllOrders() {
//        return orderRepository.findAll();
//    }
//
//    public void markOrderAsShipped(Long orderId) {
////        Order order = orderRepository.findById(orderId).orElse(null);
////        if (order != null) {
////            order.setStatus("shipped");
////            orderRepository.save(order);
////        }
//    }
//}

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.model.Order;
import com.dao.OrderRepository;
import java.util.List;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    public Order createOrder(Order order) {
        return orderRepository.save(order);
    }

    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    public void markOrderAsShipped(Long orderId) {
        // Your logic to mark order as shipped
    }
}

