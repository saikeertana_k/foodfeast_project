package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Product;

@Service
public class ProductDAO {
 
	@Autowired
	private ProductRepository productRepo;

	public void register(Product product) {
		// TODO Auto-generated method stub
		productRepo.save(product);
		
	}

	public List<Product> getAllProducts() {
		// TODO Auto-generated method stub
		return productRepo.findAll();
	}
	
	public void deleteProductById(int id) {
	    productRepo.deleteById(id);
	}

//	public void updateProduct(Product product) {
//	    productRepo.save(product);
//	}
	
	public Product updateProduct(Product product) {
	    
	    return productRepo.save(product);
	}


	public List<Product> getProductsByCategory(String categoryName) {
        return productRepo.findByCategory(categoryName);
    }

}
