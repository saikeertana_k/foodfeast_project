package com.model;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import javax.persistence.*;

@Entity
@Table(name = "orders")
public class Order {
    
    @Id
    
    private Long id;

//    @ManyToOne
//    @JoinColumn(name = "user_id", nullable = false)
//    private Users owner;

    @Column(columnDefinition = "TEXT")
    private String products;

//    @Column(nullable = false, columnDefinition = "VARCHAR(255) DEFAULT 'processing'")
//    private String status;

    @Column(nullable = false)
    private double total;

    @Column(nullable = false)
    private int count;

    @Column(nullable = false)
    private String date;

    private String address;

    private String country;

	public Order() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Order(Long id, String products, double total, int count, String date, String address, String country) {
		super();
		this.id = id;
		this.products = products;
		this.total = total;
		this.count = count;
		this.date = date;
		this.address = address;
		this.country = country;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProducts() {
		return products;
	}

	public void setProducts(String products) {
		this.products = products;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	
    // Constructors, getters, and setters
    
	@Override
	public String toString() {
		return "Order [id=" + id + ", products=" + products + ", total=" + total + ", count=" + count + ", date=" + date
				+ ", address=" + address + ", country=" + country + "]";
	}

	public void generateRandomId() {
        this.id = ThreadLocalRandom.current().nextLong(10000, 100000);
    }
}
