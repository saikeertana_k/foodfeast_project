package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.ProductDAO;
import com.model.Product;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class ProductController {
    
    @Autowired
    private ProductDAO productDao;

    @PostMapping("/registerProduct")
    public void register(@RequestBody Product product) {
        System.out.println("Data received: " + product);
        productDao.register(product);
    }
    
    @RequestMapping("showAllProducts")
    public List<Product> showAllProducts(){
    	return productDao.getAllProducts();
    }
    
    @DeleteMapping("/deleteProduct/{id}")
    public void deleteProduct(@PathVariable int id) {
        productDao.deleteProductById(id);
    }
    
//    @PutMapping("/editProduct/{id}")
//    public void editProduct(@PathVariable int id, @RequestBody Product product) {
//        // Ensure that the product ID matches the path variable ID
//        if (product.getId() == id) {
//            productDao.updateProduct(product);
//        }
//    }
    
    @PutMapping("/editProduct/{id}")
    public ResponseEntity<Product> editProduct(@PathVariable int id, @RequestBody Product product) {
       
        if (product.getId() == id) {
            Product updatedProduct = productDao.updateProduct(product);
            return ResponseEntity.ok(updatedProduct);
        } else {
            return ResponseEntity.badRequest().build(); // Or handle the error accordingly
        }
    }

    
    @GetMapping("/productsByCategory/{category}")
    public List<Product> getProductsByCategory(@PathVariable String category) {
        return productDao.getProductsByCategory(category);
    }
    
    

    
}
