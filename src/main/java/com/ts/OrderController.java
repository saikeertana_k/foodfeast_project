package com.ts;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//import com.model.Order;
//import com.dao.OrderService;
//// Import the OrderService here
//
//import java.util.List;
//
//@RestController
//@RequestMapping("/orders")
//public class OrderController {
//
//    @Autowired
//    private OrderService orderService;
//
//    @PostMapping("/")
//    public ResponseEntity<Order> createOrder(@RequestBody Order order) {
//        Order createdOrder = orderService.createOrder(order);
//        return ResponseEntity.ok(createdOrder);
//    }
//
//    @GetMapping("/")
//    public ResponseEntity<List<Order>> getAllOrders() {
//        List<Order> orders = orderService.getAllOrders();
//        return ResponseEntity.ok(orders);
//    }
//
//    @PatchMapping("/{id}/mark-shipped")
//    public ResponseEntity<List<Order>> markOrderAsShipped(@PathVariable("id") Long orderId) {
//        orderService.markOrderAsShipped(orderId);
//        List<Order> orders = orderService.getAllOrders();
//        return ResponseEntity.ok(orders);
//    }
//}


//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//import com.model.Order;
//import com.dao.OrderService;
//import com.stripe.Stripe;
//import com.stripe.exception.StripeException;
//import com.stripe.model.PaymentIntent;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//@CrossOrigin(origins = "http://localhost:4200")
//@RestController
//@RequestMapping("/orders")
//public class OrderController {
//
//    @Autowired
//    private OrderService orderService;
//
//    @PostMapping("/")
//    public ResponseEntity<Order> createOrder(@RequestBody Order order) {
//        Order createdOrder = orderService.createOrder(order);
//        return ResponseEntity.ok(createdOrder);
//    }
//
//    @GetMapping("/")
//    public ResponseEntity<List<Order>> getAllOrders() {
//        List<Order> orders = orderService.getAllOrders();
//        return ResponseEntity.ok(orders);
//    }
//
//    @PatchMapping("/{id}/mark-shipped")
//    public ResponseEntity<List<Order>> markOrderAsShipped(@PathVariable("id") Long orderId) {
//        orderService.markOrderAsShipped(orderId);
//        List<Order> orders = orderService.getAllOrders();
//        return ResponseEntity.ok(orders);
//    }
//
//    @PostMapping("/create-payment-intent")
//    public ResponseEntity<String> createPaymentIntent(@RequestParam Double amount) {
//        try {
//            Stripe.apiKey = "sk_test_51Ol3b7SGBRkKqNj450brI6QX4bBmiStRxNvn7AymnqxTZmRSHCG5NtOlzT77RMojs8X1kCsZn3dPTHv3FANSGx7Q001KjhK3q6";
//            Map<String, Object> params = new HashMap<>();
//            params.put("amount", (int) (amount * 100)); // Amount in cents
//            params.put("currency", "usd");
//            PaymentIntent paymentIntent = PaymentIntent.create(params);
//            return ResponseEntity.ok(paymentIntent.getClientSecret());
//        } catch (StripeException e) {
//            e.printStackTrace();
//            return ResponseEntity.badRequest().body("Failed to create payment intent");
//        }
//    }
//}
//


//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//import com.model.Order;
//import com.dao.OrderService;
//import com.stripe.Stripe;
//import com.stripe.exception.StripeException;
//import com.stripe.model.PaymentIntent;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//@CrossOrigin(origins = "http://localhost:4200") // Allow requests from Angular application origin
//@RestController
//@RequestMapping("/orders")
//public class OrderController {
//
//    @Autowired
//    private OrderService orderService;
//
//    @PostMapping("/")
//    public ResponseEntity<Order> createOrder(@RequestBody Order order) {
//        Order createdOrder = orderService.createOrder(order);
//        return ResponseEntity.ok(createdOrder);
//    }
//
//    @GetMapping("/")
//    public ResponseEntity<List<Order>> getAllOrders() {
//        List<Order> orders = orderService.getAllOrders();
//        return ResponseEntity.ok(orders);
//    }
//
//    @PatchMapping("/{id}/mark-shipped")
//    public ResponseEntity<List<Order>> markOrderAsShipped(@PathVariable("id") Long orderId) {
//        orderService.markOrderAsShipped(orderId);
//        List<Order> orders = orderService.getAllOrders();
//        return ResponseEntity.ok(orders);
//    }
//
//    @PostMapping("/create-payment-intent")
//    public ResponseEntity<String> createPaymentIntent(@RequestParam Double amount) {
//        try {
//            Stripe.apiKey = "sk_test_51Ol3b7SGBRkKqNj450brI6QX4bBmiStRxNvn7AymnqxTZmRSHCG5NtOlzT77RMojs8X1kCsZn3dPTHv3FANSGx7Q001KjhK3q6";
//            Map<String, Object> params = new HashMap<>();
//            params.put("amount", (int) (amount * 100)); // Amount in cents
//            params.put("currency", "usd");
//            PaymentIntent paymentIntent = PaymentIntent.create(params);
//            return ResponseEntity.ok(paymentIntent.getClientSecret());
//        } catch (StripeException e) {
//            e.printStackTrace();
//            return ResponseEntity.badRequest().body("Failed to create payment intent");
//        }
//    }
//}



//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//import com.model.Order;
//import com.dao.OrderService;
//import com.stripe.Stripe;
//import com.stripe.exception.StripeException;
//import com.stripe.model.PaymentIntent;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//@RestController
//@RequestMapping("/orders")
//public class OrderController {
//
//    @Autowired
//    private OrderService orderService;
//
//    // Add @CrossOrigin annotation to allow requests from http://localhost:4200
//    @CrossOrigin(origins = "http://localhost:4200")
//    @PostMapping("/")
//    public ResponseEntity<Order> createOrder(@RequestBody Order order) {
//        Order createdOrder = orderService.createOrder(order);
//        return ResponseEntity.ok(createdOrder);
//    }
//
//    // Add @CrossOrigin annotation to allow requests from http://localhost:4200
//    @CrossOrigin(origins = "http://localhost:4200")
//    @GetMapping("/")
//    public ResponseEntity<List<Order>> getAllOrders() {
//        List<Order> orders = orderService.getAllOrders();
//        return ResponseEntity.ok(orders);
//    }
//
//    // Add @CrossOrigin annotation to allow requests from http://localhost:4200
//    @CrossOrigin(origins = "http://localhost:4200")
//    @PatchMapping("/{id}/mark-shipped")
//    public ResponseEntity<List<Order>> markOrderAsShipped(@PathVariable("id") Long orderId) {
//        orderService.markOrderAsShipped(orderId);
//        List<Order> orders = orderService.getAllOrders();
//        return ResponseEntity.ok(orders);
//    }
//
//    // Add @CrossOrigin annotation to allow requests from http://localhost:4200
//    @CrossOrigin(origins = "http://localhost:4200")
//    @PostMapping("/create-payment-intent")
//    public ResponseEntity<String> createPaymentIntent(@RequestParam Double amount) {
//        try {
//            Stripe.apiKey = "sk_test_51Ol3b7SGBRkKqNj450brI6QX4bBmiStRxNvn7AymnqxTZmRSHCG5NtOlzT77RMojs8X1kCsZn3dPTHv3FANSGx7Q001KjhK3q6";
//            Map<String, Object> params = new HashMap<>();
//            params.put("amount", (int) (amount * 100)); // Amount in cents
//            params.put("currency", "usd");
//            PaymentIntent paymentIntent = PaymentIntent.create(params);
//            return ResponseEntity.ok(paymentIntent.getClientSecret());
//        } catch (StripeException e) {
//            e.printStackTrace();
//            return ResponseEntity.badRequest().body("Failed to create payment intent");
//        }
//    }
//}


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.model.Order;
import com.dao.OrderService;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.PaymentIntent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/orders")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/")
    public ResponseEntity<Order> createOrder(@RequestBody Order order) {
        Order createdOrder = orderService.createOrder(order);
        return ResponseEntity.ok(createdOrder);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/")
    public ResponseEntity<List<Order>> getAllOrders() {
        List<Order> orders = orderService.getAllOrders();
        return ResponseEntity.ok(orders);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PatchMapping("/{id}/mark-shipped")
    public ResponseEntity<List<Order>> markOrderAsShipped(@PathVariable("id") Long orderId) {
        orderService.markOrderAsShipped(orderId);
        List<Order> orders = orderService.getAllOrders();
        return ResponseEntity.ok(orders);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/create-payment-intent")
    public ResponseEntity<?> createPaymentIntent(@RequestParam Double amount) {
        try {
            Stripe.apiKey = "sk_test_51Ol3b7SGBRkKqNj450brI6QX4bBmiStRxNvn7AymnqxTZmRSHCG5NtOlzT77RMojs8X1kCsZn3dPTHv3FANSGx7Q001KjhK3q6";
            Map<String, Object> params = new HashMap<>();
            params.put("amount", (int) (amount * 100)); // Amount in cents
            params.put("currency", "usd");
            PaymentIntent paymentIntent = PaymentIntent.create(params);
            
            // Create a map to hold the client secret
            Map<String, String> responseData = new HashMap<>();
            responseData.put("clientSecret", paymentIntent.getClientSecret());

            // Return the map as JSON in the response body
            return ResponseEntity.ok(responseData);
        } catch (StripeException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to create payment intent");
        }
    }
}



