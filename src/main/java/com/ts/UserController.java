package com.ts;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.UserDAO;
import com.model.Users;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class UserController {

    @Autowired
    private UserDAO userDAO;

    @PostMapping("/registerUser")
    public Users registerUser(@RequestBody Users user) {
        return userDAO.registerUser(user);
    }

    @PostMapping("/login")
    public ResponseEntity<Map<String, Boolean>> loginUser(@RequestBody Users user) {
        System.out.println("Received login request for email: " + user.getEmailId());
        
        boolean isAuthenticated = userDAO.authenticateUser(user.getEmailId(), user.getPassword());

        if (isAuthenticated) {
            System.out.println("Authentication successful for email: " + user.getEmailId());
        } else {
            System.out.println("Authentication failed for email: " + user.getEmailId());
        }

        Map<String, Boolean> response = new HashMap<>();
        response.put("success", isAuthenticated);

        return ResponseEntity.ok(response);
    }
    
    @GetMapping("/users")
    public List<Users> getAllUsers() {
        return userDAO.getAllUsers();
    }
    
    
}
